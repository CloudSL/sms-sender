# sms-sender

A simple SMS Sender application that receive the message, reciever and Sender ID from AWS API gateway and use AWS Lambda & SNS to send the SMS

example for POST request JSON

{
    "message": "Hello! This is an automated message from Digital Pulz",
    "sender": "DigitalPulz",
    "receiver": "+9400000000"
}